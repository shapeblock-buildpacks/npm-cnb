package npm

import (
	"fmt"
	"io/ioutil"
	"path/filepath"

	"github.com/paketo-buildpacks/packit"
	"gopkg.in/yaml.v2"
)

type Dependency = map[string]string

type PlatformConfig struct {
	Name  string `yaml:"name"`
	Type  string `yaml:"type"`
	Build struct {
		Flavor string `yaml:"flavor"`
	} `yaml:"build,omitempty"`
	Web struct {
		Commands struct {
			Start string `yaml:"start"`
		} `yaml:"commands"`
	} `yaml:"web"`
	Hooks struct {
		Build string `yaml:"build"`
	} `yaml:"hooks"`
	Dependencies map[string]Dependency  `yaml:"dependencies"`
	X            map[string]interface{} `yaml:"-"`
}

func Detect() packit.DetectFunc {
	return func(context packit.DetectContext) (packit.DetectResult, error) {

		yamlFile, err := ioutil.ReadFile(filepath.Join(context.WorkingDir, ".platform.app.yaml"))
		if err != nil {
			return packit.DetectResult{}, err
		}
		var config PlatformConfig

		err = yaml.Unmarshal(yamlFile, &config)
		if err != nil {
			return packit.DetectResult{}, err
		}

		fmt.Printf("Build flavor -> %s\n", config.Build.Flavor)
		if config.Build.Flavor == "none" {
			return packit.DetectResult{}, fmt.Errorf("not npm")
		}

		return packit.DetectResult{
			Plan: packit.BuildPlan{
				Provides: []packit.BuildPlanProvision{
					{Name: "npm"},
				},
				Requires: []packit.BuildPlanRequirement{
					{
						Name: "npm",
					},
				},
			},
		}, nil
	}
}
