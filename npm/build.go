package npm

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/paketo-buildpacks/packit"
	"gopkg.in/yaml.v2"
)

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func Build() packit.BuildFunc {
	return func(context packit.BuildContext) (packit.BuildResult, error) {

		// fix caching
		fmt.Printf("Running npm install\n")

		npmLayer, err := context.Layers.Get("npm")
		if err != nil {
			return packit.BuildResult{}, err
		}

		yamlFile, err := ioutil.ReadFile(filepath.Join(context.WorkingDir, ".platform.app.yaml"))
		if err != nil {
			return packit.BuildResult{}, err
		}
		var config PlatformConfig

		err = yaml.Unmarshal(yamlFile, &config)
		if err != nil {
			return packit.BuildResult{}, err
		}

		cacheDir := filepath.Join(npmLayer.Path, "npm-cache")

		npmLayer.Build = true
		npmLayer.Launch = true
		npmLayer.Cache = true

		binPath := strings.Join([]string{os.Getenv("PATH"), filepath.Join(os.Getenv("NODE_HOME"), "bin")}, string(os.PathListSeparator))
		err = os.Setenv("PATH", binPath)
		if err != nil {
			return packit.BuildResult{}, err
		}

		fmt.Printf("Running dependencies\n")

		for library, version := range config.Dependencies["nodejs"] {
			dependency := fmt.Sprintf("%s@%s", library, version)
			fmt.Printf("Dependency -> %s\n", dependency)
			cmd := exec.Command("npm", "install", "--no-package-lock", "--no-save", dependency, "--unsafe-perm", "--cache", cacheDir)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			if err = cmd.Run(); err != nil {
				return packit.BuildResult{}, err
			}
		}
		workingDirNodeModules := filepath.Join(context.WorkingDir, "node_modules")
		if fileExists(workingDirNodeModules) {
			fmt.Printf("Copy node modules\n")
			// copy node_modules to npmLayer Path, first after installng dependencies
			cmd := exec.Command("cp", "-r", workingDirNodeModules, npmLayer.Path)
			if err = cmd.Run(); err != nil {
				return packit.BuildResult{}, err
			}
		}

		npmLayerNodeModules := filepath.Join(npmLayer.Path, "node_modules")
		npmBinPath := strings.Join([]string{os.Getenv("PATH"), filepath.Join(npmLayerNodeModules, ".bin")}, string(os.PathListSeparator))
		err = os.Setenv("PATH", npmBinPath)
		fmt.Printf("PATH -> %s\n", os.Getenv("PATH"))
		if err != nil {
			return packit.BuildResult{}, err
		}
		if config.Hooks.Build != "" {
			fmt.Printf("Running build hook ->\n")
			buildScript := filepath.Join(npmLayer.Path, "build")
			err = ioutil.WriteFile(buildScript, []byte(config.Hooks.Build), 0644)
			if err != nil {
				return packit.BuildResult{}, err
			}

			cmd := exec.Command("bash", buildScript)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			if err = cmd.Run(); err != nil {
				return packit.BuildResult{}, err
			}
		} else {
			fmt.Printf("Running npm install ->\n")
			cmd := exec.Command("npm", "install", "--unsafe-perm", "--cache", cacheDir)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			if err = cmd.Run(); err != nil {
				return packit.BuildResult{}, err
			}
		}

		// Copy again after running build hooks/npm install.
		cmd := exec.Command("cp", "-r", workingDirNodeModules, npmLayer.Path)
		if err = cmd.Run(); err != nil {
			return packit.BuildResult{}, err
		}

		fmt.Printf("Layer path -> %s\n", npmLayer.Path)

		path := filepath.Join(npmLayer.Path, "node_modules", ".bin")
		npmLayer.SharedEnv.Append("PATH", path, string(os.PathListSeparator))

		buildResult := packit.BuildResult{
			Plan: context.Plan,
			Layers: []packit.Layer{
				npmLayer,
			},
		}
		return buildResult, nil
	}
}
