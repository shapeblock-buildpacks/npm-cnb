package main

import (
	"gitlab.com/shapeblock-buildpacks/npm-cnb/npm"

	"github.com/paketo-buildpacks/packit"
)

func main() {
	packit.Detect(npm.Detect())
}
